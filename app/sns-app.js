const gitapp = require('./git-app');
const snsService = require('../service/sns-service');
const appConstants = require('../constants/app-constants.js');
var envPropertiesResponse;
var snsServiceResponse = {};


class SNSApp {
    constructor(config) {
        this.config = config;
    }

    async checkSNSConfigForEnvAndModule(env, module) {
        envPropertiesResponse = await gitapp.validateEnvPropertiesForEnvAndModule(env, module);

        switch (module) {
            case 'unifiedConnector':
                var topicARNKey = appConstants.S3.PNP.EVENT_NOTIFICATION_TARGET_TOPIC_NAME;
                await this.validateSNSsetupForGivenTopicARN(topicARNKey, env);
                break;
        }
        return snsServiceResponse;;
    }

    async validateSNSsetupForGivenTopicARN(topicARNKey, env) {
        var appConstantArray = [];
        appConstantArray.push(topicARNKey);
        var propertiesAvailable = this.checkAppConstantsInEnvPropertiesResponse(appConstantArray);
        if (propertiesAvailable) {
            try {
                snsServiceResponse = await snsService.checkIfSNSSubscribedInTopicForEnv(this.config, envPropertiesResponse[topicARNKey],
                    env);
            } catch (error) {
                snsServiceResponse = error;
            }
        } else {
            snsServiceResponse.error = 'Missing Properties in git conf';
        }
        return snsServiceResponse;
    }

    /* check if appConstants has values in envPropertiesResultMap */
    checkAppConstantsInEnvPropertiesResponse(appConstantArray) {

        var arePropertiesAvailable = true;
        for (let propertyKey of appConstantArray) {
            console.log(propertyKey);
            if (envPropertiesResponse[propertyKey] === 'Not available in git conf') {
                arePropertiesAvailable = false;
            }
        }
        return arePropertiesAvailable;
    }

}

module.exports = SNSApp;