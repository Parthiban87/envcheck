var AWS = require('aws-sdk');
const awsConfig = require('../config/aws-config.json');
var S3App = require('./s3-app');
var SNSApp = require('./sns-app');

class AWSApp {

  constructor() {
    this.config = new AWS.Config({
      accessKeyId: awsConfig.accessKeyId,
      secretAccessKey: awsConfig.secretAccessKey,
      region: awsConfig.region
    });

    //create Instance for s3,sns,
    this.s3AppInstance = new S3App(this.config);
    this.snsAppInstance = new SNSApp(this.config);
  }

  getApp(appName) {
    let app = null;
    if(appName === 's3') {
      app =  this.s3AppInstance;
    } else if(appName === 'sns') {
      app =  this.snsAppInstance;
    }
    return app;
  }
}

module.exports = AWSApp;


