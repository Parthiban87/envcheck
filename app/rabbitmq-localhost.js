
var amqp = require('amqplib/callback_api');
var amqpConn = null;

function bail(err) {
    console.error(err);
    process.exit(1);
}

amqp.connect(
    'amqp://localhost',
    function(err, conn) {
        if(conn !=null)
            console.log(conn);

        if (err != null) {
            bail(error);
        }
        conn.on("error", function (err) {
            if (err.message !== "Connection closing") {
            console.error("[AMQP] conn error", err.message);
            }
        });
        conn.on("close", function () {
            console.error("[AMQP] reconnecting");
            setTimeout(start, 1000);
        });
        console.log("[AMQP] connected");
    });


