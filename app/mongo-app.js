var _ = require('lodash');
const appConstants = require('../constants/app-constants.js');
const gitService = require('../service/git-service.js');
const mongoService = require('../service/mongo-service');
const mongoTables = require('../config/mongo-tables.json');
const configReader = require('./config-reader');
var envPropertiesFromGit;


function generateMongoServerURL(envPropertiesFromGit, db) {

    var host, port, database, user, password, url;

    if (appConstants.MONGO[db].PORT) {
        var primaryAndSecondaryhosts = envPropertiesFromGit.get(appConstants.MONGO[db].HOST);
        hostArray = _.split(primaryAndSecondaryhosts, ',', 2);
        host = hostArray[1];
        var ports = envPropertiesFromGit.get(appConstants.MONGO[db].PORT);
        portArray = _.split(ports, ',', 1);
        port = portArray[0];
    } else {
        var primaryAndSecondaryhostsAndPorts = envPropertiesFromGit.get(appConstants.MONGO[db].HOST);
        hostAndPortArray = _.split(primaryAndSecondaryhostsAndPorts, ',', 2);
        hostAndPort = _.split(hostAndPortArray[1], ':', 2);
        host = hostAndPort[0];
        port = hostAndPort[1];
    }

    database = envPropertiesFromGit.get(appConstants.MONGO[db].DATABASE);
    user = envPropertiesFromGit.get(appConstants.MONGO[db].USER);
    password = envPropertiesFromGit.get(appConstants.MONGO[db].PASSWORD);

    url = `mongodb://${user}:${password}@${host}:${port}/${database}`;

    return url;
}


function getMongoConnectionResponseObjForAllDBs(env) {

    return new Promise(function (resolve, reject) {

        gitService.getGitConfigForEnvironment(env, result => {
            envPropertiesFromGit = result;
        });

        getMongoConnectionStatusForAllDB().then(mongoConnectionResponse => {
            resolve(mongoConnectionResponse);
        }, (error) => {
            reject(error);
        });
    });


    function getMongoConnectionStatusForAllDB() {

        return new Promise(async function (resolve, reject) {
            var mongoConnectionResponse = {};
            //Iterate the Mongo Object
            for (const key in appConstants.MONGO) {
                console.log(key);

                await getMongoConnectionStatusForGivenDB(key).then(success => {
                    mongoConnectionResponse[`${key}`] = true;
                },
                    (failure) => {
                        mongoConnectionResponse[`${key}`] = false;
                    });

                if (Object.keys(appConstants.MONGO).length === Object.keys(mongoConnectionResponse).length) {
                    resolve(mongoConnectionResponse);
                }
            }

            if (Object.keys(appConstants.MONGO).length !== Object.keys(mongoConnectionResponse).length) {
                reject({ 'error': 'Error in generating mongo connection status' });
            }
        });
    }


    function getMongoConnectionStatusForGivenDB(key) {

        var url = generateMongoServerURL(envPropertiesFromGit, key);
        return mongoService.checkMongoConnection(url);

    }

}


function checkForCollectionExists(env, module) {

    gitService.getGitConfigForEnvironment(env, result => {
        envPropertiesFromGit = result;
    });
    return getcollectionExistsResponse(module);
}

async function getcollectionExistsResponse(module) {
    var collectionExistsResponse = { 'db': {} };
    switch (module) {
        case 'unifiedConnector':
            for (const key in mongoTables.db) {
                console.log(key);
                var obj = mongoTables.db[key];
                if (module in obj) {
                    //prepare URL for the DB
                    var url = await generateMongoServerURL(envPropertiesFromGit, _.toUpper(key));
                    var collectionsToCheck = _.split(obj[module], ',');

                    var collectionExistsObject = await mongoService.checkForCollectionExists(url, collectionsToCheck);
                    collectionExistsResponse.db[key] = collectionExistsObject;
                    // console.log(collectionExistsResponse);
                }
            }
            break;
    }
    return collectionExistsResponse
}

module.exports = {
    getMongoConnectionResponseObjForAllDBs,
    checkForCollectionExists
}