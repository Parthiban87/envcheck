var AWSApp = require('./aws-app');
const gitapp = require('./git-app');
const s3Service = require('../service/s3-service');
const appConstants = require('../constants/app-constants');
var envPropertiesResponse;
var s3Response = {};

class S3App {

    constructor(config) {
        this.config = config;
    }

    async checkS3BucketHasEventConfigForModule(env, module) {
        envPropertiesResponse = await gitapp.validateEnvPropertiesForEnvAndModule(env, module);
        // console.log(envPropertiesResultMap);

        switch (module) {
            case 'unifiedConnector':

                var unifiedConnectorConstantsArray = [];
                unifiedConnectorConstantsArray.push(appConstants.S3.PNP.BUCKET_NAME);
                unifiedConnectorConstantsArray.push(appConstants.S3.PNP.EVENT_NOTIFICATION_TARGET_TOPIC_NAME);
                s3Response = await this.validates3BucketAndSNSsetup(unifiedConnectorConstantsArray);
                console.log(s3Response);
                break;
        }

        return s3Response;
    }

    async validates3BucketAndSNSsetup(appConstantArray) {
        var bucketNameKey = appConstantArray[0], topicNameKey = appConstantArray[1];
        var s3ServiceResponse = {};
        var propertiesAvailable = this.checkAppConstantsInEnvPropertiesResponse(appConstantArray);
        if (propertiesAvailable) {
            try {
                s3ServiceResponse = await s3Service.checkIfBucketHasEventConfigured(this.config, envPropertiesResponse[bucketNameKey],
                    envPropertiesResponse[topicNameKey]);
            } catch (error) {
                s3ServiceResponse = error;
            }
        } else {
            s3ServiceResponse.error = 'Missing Properties in git conf';
        }
        return s3ServiceResponse;
    }

/* check if appConstants has values in envPropertiesResultMap */
checkAppConstantsInEnvPropertiesResponse(appConstantArray) {

    var arePropertiesAvailable = true;
    for (let propertyKey of appConstantArray) {
        console.log(propertyKey);
        if (envPropertiesResponse[propertyKey] === 'Not available in git conf') {
            arePropertiesAvailable = false;
        }
    }
    return arePropertiesAvailable;
}
}

module.exports = S3App;
