var _ = require('lodash');
const appConstants = require('../constants/app-constants.js');
const gitService = require('../service/git-service.js');
const postgresService = require('../service/postgres-service');
const postgresTables = require('../config/postgres-tables.json');
var envPropertiesFromGit;
var postgresConfig = null;

function checkPostgresConnection(env) {

        gitService.getGitConfigForEnvironment(env, result => {
            envPropertiesFromGit = result;
        });

        var hostAndPortArray = _.split(envPropertiesFromGit.get(appConstants.PG.HOST), ':', 2);
        postgresConfig = {
            host: hostAndPortArray[0],
            port: hostAndPortArray[1],
            database: envPropertiesFromGit.get(appConstants.PG.DATABASE),
            user: envPropertiesFromGit.get(appConstants.PG.USER),
            password: envPropertiesFromGit.get(appConstants.PG.PASSWORD),
            ssl: envPropertiesFromGit.get(appConstants.PG.SSL),
        }

        return postgresService.connectToPostgres(postgresConfig);
        
}

async function checkPostgresTablesExists(env, module) {
    var postgresTablesResponse;
    try {
        await checkPostgresConnection(env);

        switch (module) {
            case 'unifiedConnector':
                unifiedConnectorTablesArrayList = postgresTables.unifiedConnector;
                try{
                    postgresTablesResponse = await postgresService.checkIfPostgresTableExists(postgresConfig, unifiedConnectorTablesArrayList);
                }catch(err){
                    postgresTablesResponse = err;
                }
                console.log(postgresTablesResponse);
                break;
        }
    } catch (err) {
        postgresTablesResponse = "Error in connecting postgres"
    }
    return postgresTablesResponse;
}

module.exports = {
    checkPostgresConnection,
    checkPostgresTablesExists
}