const gitService = require('../service/git-service.js');
const requiredEnvPropertiesFromFile = require('../config/env-properties.json');
const gitHubConfig = require('../config/git-config.json');
var envPropertiesFromGit;
var requiredEnvPropertiesArray = [];
var envPropertiesResponse = {};
const octokit = require('@octokit/rest')()


function validateEnvPropertiesForEnvAndModule(env, module) {
    switch (module) {
        case 'unifiedConnector':
            requiredEnvPropertiesArray = requiredEnvPropertiesFromFile.unifiedConnector;
            break;
        default:
            console.log('No Matching Apps Found')
    }

    gitService.getGitConfigForEnvironment(env,result => {

        envPropertiesFromGit = result;

        // Validate if required env properties are present in Env Config.
        for (let key of requiredEnvPropertiesArray) {
            if (envPropertiesFromGit.get(key)) {
                //console.log(`Required Property  ${key}  is available in  ${env} Conf file`);
                envPropertiesResponse[key] = envPropertiesFromGit.get(key);
            } else {
                //console.log(`Required Property  ${key} is NOT available in ${env} Conf file`);
                envPropertiesResponse[key] = "Not available in git conf"
            }
        }

    });
    return envPropertiesResponse;
}

function authenticateGit() {
    gitService.authenticateGit(gitHubConfig.token);
}

function getconfFile(env, getconfFilecallback) {
    gitService.getLatestGitConfigForEnvironment(env, gitHubConfig, gitConfigCallback => {
        getconfFilecallback(gitConfigCallback);
    });

}


module.exports = {
    validateEnvPropertiesForEnvAndModule,
    authenticateGit,
    getconfFile
}

