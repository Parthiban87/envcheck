const herokuService = require('../service/heroku-service.js');
const herokuConfig = require('../config/heroku-config.json');

var appsFromConfig = [];

async function checkHerokuAppsForEnvAndModule(Env, Module) {

    switch (Module) {
        case 'unifiedConnector':
            appsFromConfig = herokuConfig.modules.unifiedConnector;
            break;
        default:
            console.log('No Matching Apps Found')
    }
    var allHerokuApps;
    var herokuAppsExists = {};
    var appsArray = [];

    try{
        allHerokuApps = await herokuService.getHerokuApps();
    }catch(error){
        console.log('Error in fetching heroku apps');
        herokuAppsExists.error='Error in fetching heroku apps';
        return herokuAppsExists;
    }
    
    for (let key in allHerokuApps) {
        appsArray.push(allHerokuApps[key].name);
    }

    for (let key in appsFromConfig) {
        appsFromConfig[key] = Env + '-' + appsFromConfig[key];
    }

    for (let key in appsFromConfig) {
        //console.log(appsArray.find(name => name === appsFromConfig[key]));
        const app = appsArray.find(name => name === appsFromConfig[key]);
        if (app) {
            herokuAppsExists[appsFromConfig[key]] = true;
        } else {
            herokuAppsExists[appsFromConfig[key]] = false;
        }
    }
    // console.log(herokuAppsExists);
    return herokuAppsExists;
};

module.exports = {
    checkHerokuAppsForEnvAndModule
}

