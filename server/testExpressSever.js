var express = require('express');
var app = express();
var mongoApp = require('../app/mongo-app');
var postgresApp = require('../app/postgres-app');
var herokuApp = require('../app/heroku-app');
const gitApp = require('../app/git-app');
const AWSApp = require('../app/aws-app');
const RabbitMQ = require('../app/rabbitmq');

gitApp.authenticateGit();

app.use('/refresh', (req, res, next) => {
    console.log('req env',req, req.query.env);
    var env = req.query.env;
    gitApp.getconfFile(env, getconfFilecallback => {
        if (getconfFilecallback) {
            console.log('Git conf file downloaded successfully');
            next();
        } else {
            console.log('Error in downloading Git conf file. Cannot Proceed Further');
        }
    }); 
});
app.use('/listApps', (req, res, next) => {
    console.log('req env',req, req.query.env);
    let apps = ['mongo', 'postgres', 'postgres', 'heroku', 'git', 's3', 'sns'];
    res.send(apps);
});

app.get('/*', async (req, res) => {
    var env = req.query.env;
    var module = req.query.module;
    var app = req.query.app;
    var response = {};

    switch (app) {
        case 'mongo':
            //Get Mongo Connection Response
            response.app = 'mongo';
            try {
                response.mongoDBConnection = await mongoApp.getMongoConnectionResponseObjForAllDBs(env);
            } catch (error) {
                response.mongoDBConnection = error;
            }

            // Get Mongo Collection Exists or Not
            try {
                response.mongocollectionExists = await mongoApp.checkForCollectionExists(env, module);
            } catch (e) {
                response.mongocollectionExists = error;
            }

            res.send(response);
            break;
        case 'postgres':
            //Get Postgres Connection Response
            try {
                response.postgresDBConnection = await postgresApp.checkPostgresConnection(env);
            } catch (error) {
                response.postgresDBConnection = error;
            }

            //Get Postgres Table Exists Response
            try {
                response.postgresTableExists = await postgresApp.checkPostgresTablesExists(env, module);
            } catch (error) {
                response.postgresTableExists = error;
            }
            res.send(response);
            break;
        case 'heroku':
            //Get Heroku App Exists Response
            try {
                response.herokuAppExists = await herokuApp.checkHerokuAppsForEnvAndModule(env, module);
            } catch (error) {
                response.herokuAppExists = error;
            }
            res.send(response);
            break;
        case 'git':
            //Get Git config params
            try {
                response.gitConfigParams = await gitApp.validateEnvPropertiesForEnvAndModule(env, module);
            } catch (error) {
                response.gitConfigParams = error;
            }
            res.send(response);
            break;
        case 's3':
            //Get s3 bucket,arn response
            try {
                var awsAppInstance = new AWSApp();
                var s3AppInstance = awsAppInstance.getApp('s3');
                response.s3Response = await s3AppInstance.checkS3BucketHasEventConfigForModule(env, module);
            } catch (error) {
                response.s3Response = error;
            }
            res.send(response);
            break;
        case 'sns':
            //Get sns subscription response
            try {
                var awsAppInstance = new AWSApp();
                var snsAppInstance = awsAppInstance.getApp('sns');
                response.snsResponse = await snsAppInstance.checkSNSConfigForEnvAndModule(env, module);
            } catch (error) {
                response.snsResponse = error;
            }
            res.send(response);
            break;
        
        case 'rabbitmq':
        //Get sns subscription response
        try {
            console.log('calling this api');
            var rabbitMQAppInstance = new RabbitMQ();
            response.rabbitMQResponse = await rabbitMQAppInstance.checkRabbitMQConfigForEnvAndModule(env, module);
        } catch (error) {
            response.rabbitMQResponse = error;
        }
        process.on('uncaughtException', (err) => {
            response.rabbitMQResponse = false;
            console.log('whoops! there was an error');
         });
        console.log('response of api', response.rabbitMQResponse);
        res.send(response);
        break;
    }
});

app.listen(8000, () => console.log('Server running on 8000 port'));
