//connect to node postgres library
var pg = require('pg');


function createClient(postgresConfig) {
    const client = new pg.Client({
        user: postgresConfig.user,
        host: postgresConfig.host,
        database: postgresConfig.database,
        password: postgresConfig.password,
        port: postgresConfig.port,
        ssl: true
    });
    return client;
}

function checkIfPostgresTableExists(postgresConfig, tablesArrayList) {
    var postgresTablesResponse = {};
    const client = createClient(postgresConfig);
    client.connect();
    let numberOfTablesExecuted = 0;
    let numberOfTableToCheck = tablesArrayList.length;

    return new Promise(function(resolve,reject){
        for (let table of tablesArrayList) {
            let query = `select * from pg_tables where tablename = \'${table}\'`;
            var res = client.query(query);
            res.then((result, err) => {
                numberOfTablesExecuted++;
                if (err) {
                    console.log(err);
                    postgresTablesResponse = err;
                    reject(postgresTablesResponse);
                } else if (result.rowCount === 1) {
                    // console.log(`Table ${table} found`);
                    postgresTablesResponse[table] = true;
                } else if (result.rowCount != 1) {
                    // console.log(`Table ${table} NOT found`);
                    postgresTablesResponse[table] = false;
                }
                if (numberOfTablesExecuted === numberOfTableToCheck) {
                    endClient(client);
                    resolve(postgresTablesResponse);
                }
            });
        }
    })
}

function endClient(client) {
    client.end();
}

function connectToPostgres(postgresConfig) {

    return new Promise(function (resolve, reject) {
        const client = createClient(postgresConfig);
        client.connect().then((success) => {
            client.end();
            resolve(true);
        }, (error) => {
            console.log(error);
            reject(false);
        });
    });
}

module.exports = {
    connectToPostgres,
    checkIfPostgresTableExists
}