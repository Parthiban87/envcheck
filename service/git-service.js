var PropertiesReader = require('properties-reader');
var configDir = __dirname + '/../config';
const octokit = require('@octokit/rest')()
var base64Util = require('../utils/base64-decoder-util');
var fileUtil = require('../utils/file-util');
var _ = require('lodash');
var gitHubInstance = null;


function getGitConfigForEnvironment(env,callback) {
        var properties = PropertiesReader(`${configDir}/${env}.properties`);
        callback(properties);
}


function authenticateGit(authToken) {
    octokit.authenticate({
        type: "token",
        token: authToken
    });
}

function getLatestGitConfigForEnvironment(env, gitHubConfig, gitConfigCallback) {

    //get the conf file inside mda-config
    var contentParam = {
        owner: gitHubConfig.owner,
        repo: gitHubConfig.mdaConfig.repo,
        path: `/${env}.conf`,
        ref: gitHubConfig.mdaConfig.ref
    }

    octokit.repos.getContent(contentParam).then(result => {

        var mdaConfigContent = base64Util.decodeBase64(result.data.content);

        getGlobalOverrideEnvConf(env, gitHubConfig, globalOverrideContent => {
            if (globalOverrideContent !== 'No file exists') {
                // var finalContent = _.concat(mdaConfigContent, globalOverrideContent);
                var finalContent = mdaConfigContent + "\n" + globalOverrideContent;
            } else {
                var finalContent = mdaConfigContent;
            }

            fileUtil.createEnvPropertyFile(__dirname + '/../config', env, finalContent,fileCreateCallback=>{
                gitConfigCallback(fileCreateCallback);
            });
        });

    }, error => {
        console.log(error);
    });

    // //get the conf file inside mda-config/global-override
    function getGlobalOverrideEnvConf(env, gitHubConfig, callback) {
        contentParam = {
            owner: gitHubConfig.owner,
            repo: gitHubConfig.globalOverride.repo,
            path: `${gitHubConfig.globalOverride.path}/${env}.conf`,
            ref: gitHubConfig.globalOverride.ref
        }
        octokit.repos.getContent(contentParam).then(result => {
            callback(base64Util.decodeBase64(result.data.content));
        }, error => {
            callback("No file exists");
        });
    }

}

module.exports = {
    getGitConfigForEnvironment,
    authenticateGit,
    getLatestGitConfigForEnvironment
};