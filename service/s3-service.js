var AWS = require('aws-sdk');
var s3;
var s3Response = {};

function checkIfBucketHasEventConfigured(config, bucketName, topicARN) {

    return new Promise(function(resolve,reject){
        s3 = new AWS.S3(config);

        var params = {
            Bucket: bucketName
        };

        s3.getBucketNotificationConfiguration(params, function (err, data) {

            if (err) {
                console.log(`Error in fetching Bucket Details ${err}, ${err.stack}`);
                s3Response.error = 'Error in fetching Bucket Details';
                reject(s3Response);
            } // an error occurred
            else {
                console.log(data);
                s3Response.bucketExists = true;

                let notificationsList = data.TopicConfigurations.filter(topicConfig => {
                    return topicConfig.TopicArn === topicARN
                });

                if (notificationsList.length) {
                    console.log(`Topic ${topicARN} is configured for Bucket ${bucketName}`);
                    s3Response.topicExists = true;
                } else {
                    console.log(`Topic ${topicARN} is NOT configured for Bucket ${bucketName}`);
                    s3Response.topicExists = false;
                }
                resolve(s3Response);
            }
        });
    })
}



module.exports = {
    checkIfBucketHasEventConfigured
};

