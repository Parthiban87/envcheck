//connect to node postgres library
var mongoose = require('mongoose');

function checkMongoConnection(url) {

    return new Promise(function (resolve, reject) {

        const mongoConnection = mongoose.connect(url,
            {
                ssl: true,
                useNewUrlParser: true
            });
        mongoConnection.
            then(() => {
                console.log('URL Connected ' + url);
                resolve(true);
                // mongoose.disconnect().then(
                //     (disconnected) => {
                //         console.log('URL Disconnected ' + url);
                //         resolve(true);
                //     }, (disconnectErr) => {
                //         console.log('Error on DisConnect');
                //         reject(false);
                //     })
            },
                (err) => {
                    console.log('Error on Connect');
                    reject(false);
                });
    });
}

function checkForCollectionExists(url, collectionsToCheck) {

    var collectionExistsObject = {};

    return mongoose.connect(url,
        {
            ssl: true,
            useNewUrlParser: true
        }).then(
            (success) => {
                //console.log('Able to connect');
                var connection = mongoose.connection;
                return connection.db.collections().then(
                    (collections) => {
                        let allCollectionNames = collections.map((collection) => {
                            return collection.s.name;
                        });
                        //console.log(allCollectionNames);

                        for (const collection of collectionsToCheck) {

                            var matchCollection = allCollectionNames.find((collectionName) =>
                                collectionName === collection
                            );

                            if (matchCollection) {
                                collectionExistsObject[collection] = true;
                            } else {
                                collectionExistsObject[collection] = false;
                            }
                        }
                        // console.log({collectionExistsObject})
                        //console.log(collectionExistsObject);
                        connection.close();
                        mongoose.disconnect();
                        return collectionExistsObject;
                    },
                    (error) => {
                        console.log('No Collection Exists' + error);
                        return collectionExistsObject;
                    }
                );
            },
            (error) => {
                console.log('Not Able to connect' + error);
            }
        );

}

module.exports = {
    checkMongoConnection,
    checkForCollectionExists
}