var _ = require('lodash');
var AWS = require('aws-sdk');
var sns;
const awsConfig = require('../config/aws-config.json');
var snsResponse = {};

function checkIfSNSSubscribedInTopicForEnv(config, topicARN, env) {

    return new Promise(function (resolve, reject) {

        sns = new AWS.SNS(config);

        var params = {
            TopicArn: topicARN /* required */
        };
        sns.listSubscriptionsByTopic(params, function (err, data) {
            if (err) {
                console.log(`Error in fetching SNS for Topic. ${err}, ${err.stack}`);
                snsResponse.error = 'Error in fetching SNS for Topic';
                reject(snsResponse);
            }
            else {
                //console.log(data);
                var s3NotificationEndpoint = _.replace(awsConfig.s3NotificationEndpoint, '{{ENV}}', env);

                let isSNSsubscribed = data.Subscriptions.filter(subscription => {
                    return subscription.Endpoint === s3NotificationEndpoint
                });

                if (isSNSsubscribed.length) {
                    console.log(`Env ${env} is configured for Topic ${topicARN}`);
                    snsResponse.topicSubscription = true;
                    resolve(snsResponse);
                }
                else {
                    console.log(`Env ${env} is NOT configured for Topic ${topicARN}`);
                    snsResponse.topicSubscription = false;
                    reject(snsResponse);
                }

            }
        });

    })



}

module.exports = {
    checkIfSNSSubscribedInTopicForEnv
};