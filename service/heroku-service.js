const Heroku = require('heroku-client');
const herokuConfig  = require('../config/heroku-config.json');

const heroku = new Heroku({ token: herokuConfig.token })

function getHerokuApps() {
    return heroku.get('/apps');
}

module.exports = {
    getHerokuApps
};