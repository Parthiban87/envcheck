var base64 = require('base-64');
var utf8 = require('utf8');


function decodeBase64(contentToBeDecoded) {
    var bytes = base64.decode(contentToBeDecoded);
    var text = utf8.decode(bytes);
    //console.log(text);
    return text;
}

module.exports = {
    decodeBase64
}
