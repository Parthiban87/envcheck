var fs = require('fs');

function createEnvPropertyFile(path,fileName,content,callback){
    // fs.writeFile('./utils/message.txt', 'Hello Node.js', (err) => {
    // if (err) throw err;
    // console.log('The file has been saved!');
    // });
    fs.writeFile(`${path}/${fileName}.properties`,content,(err) =>{
        if (err) callback(false);
        console.log('The file has been saved!');
        callback(true);
        });
}

module.exports={
    createEnvPropertyFile
}  