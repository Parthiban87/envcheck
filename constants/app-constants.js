const S3 = {
    PNP: {
        BUCKET_NAME: "s3.pnp.bucket.name",
        EVENT_NOTIFICATION_TARGET_TOPIC_NAME: "s3.pnp.event.notification.target.topic.name",
    }
};

const PG = {
    HOST: "postgresql.shared.host",
    DATABASE: "postgresql.shared.db",
    USER: "postgresql.shared.username",
    PASSWORD: "postgresql.shared.password",
    SSL: "postgresql.shared.ssl.enable"
};

const MONGO = {
    GLOBAL: {
        HOST: "mongo.global.host",
        PORT: "mongo.global.port",
        DATABASE: "mongo.global.db",
        USER: "mongo.global.username",
        PASSWORD: "mongo.global.password",
        SSL: "mongo.global.ssl.enable"
    },
    AUTH: {
        HOST: "mongo.auth.host",
        PORT: "mongo.auth.port",
        DATABASE: "mongo.auth.db",
        USER: "mongo.auth.username",
        PASSWORD: "mongo.auth.password",
        SSL: "mongo.auth.ssl.enable"
    },
    DATA: {
        HOST: "data.mongo.shared.host_port",
        DATABASE: "data.mongo.shared.db",
        USER: "data.mongo.shared.username",
        PASSWORD: "data.mongo.shared.password",
        SSL: "data.mongo.shared.ssl.enable"
    },
    SCHEMA: {
        HOST: "schema.mongo.shared.host_port",
        DATABASE: "schema.mongo.shared.db",
        USER: "schema.mongo.shared.username",
        PASSWORD: "schema.mongo.shared.password",
        SSL: "schema.mongo.shared.ssl.enable"
    }

}

const RABBITMQ = {
    QUEUE : 'gainsight-uc-mediator-queue'
};

const config = {
    S3,
    PG,
    MONGO,
    RABBITMQ
}

module.exports = Object.freeze(config);